package no.uib.inf101.sem2.game.controller;

import java.awt.event.KeyListener;

import no.uib.inf101.sem2.game.view.GameView;
import no.uib.inf101.sem2.model.GameState;

import java.awt.event.KeyEvent;

public class GameController implements KeyListener {

    private ControllableGame controllableGame;
    private GameView gameView;

    public GameController(ControllableGame controllableGame, GameView gameView) {
        this.controllableGame = controllableGame;
        this.gameView = gameView;
        gameView.addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (controllableGame.getGameState() == GameState.START_GAME) {
            controllableGame.startGame();
            gameView.repaint();
            return;
        }
        if (controllableGame.getGameState() == GameState.ACTIVE_GAME) {

            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                controllableGame.moveLeft();

            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                controllableGame.moveRight();

            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                controllableGame.moveDown();

            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                controllableGame.moveUp();
            } else {
                return;
            }
            controllableGame.placeRandomTile();
            gameView.repaint();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
