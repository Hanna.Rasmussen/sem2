package no.uib.inf101.sem2.game.view;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.Tile;

public interface ViewableGameModel {

    /**
     * 
     * @return dimensions of the grid
     */
    GridDimension getGridDimension();

    /**
     * 
     * @return the tiles on the board
     */
    Iterable<GridCell<Tile>> getTilesOnBoard();

    /**
     * @return if game is active or over
     */
    GameState getGameState();
}
