package no.uib.inf101.sem2.game.controller;

import no.uib.inf101.sem2.model.GameState;

public interface ControllableGame {

    /**
     * starts the game
     * changes the game state to active
     */
    void startGame();

    /**
     * moves all the tiles on the board up
     * and combines equal tiles
     */
    void moveUp();

    /**
     * moves all the tiles on the board down
     * and combines equal tiles
     */
    void moveDown();

    /**
     * moves all the tiles on the board to the left
     * and combines equal tiles
     */
    void moveLeft();

    /**
     * moves all the tiles on the board to the right
     * and combines equal tiles
     */
    void moveRight();

    /**
     * places a tile with value of 2 or 4 a random place at the board
     * if the board is full, nothing happens
     */
    void placeRandomTile();

    /**
     * @return the state of the game
     */
    GameState getGameState();

}
