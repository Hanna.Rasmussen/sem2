package no.uib.inf101.sem2.game.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme {

    @Override
    public Color getTileColor(int value) {
        Color tileColor;
        switch (value) {
            case 0 -> tileColor = new Color(0xcdc1b4);
            case 2 -> tileColor = new Color(0xeee4da);
            case 4 -> tileColor = new Color(0xede0c8);
            case 8 -> tileColor = new Color(0xf2b179);
            case 16 -> tileColor = new Color(0xf59563);
            case 32 -> tileColor = new Color(0xf67c5f);
            case 64 -> tileColor = new Color(0xf65e3b);
            case 128 -> tileColor = new Color(0xedcf72);
            case 256 -> tileColor = new Color(0xedcc61);
            case 512 -> tileColor = new Color(0xedc850);
            case 1024 -> tileColor = new Color(0xedc53f);
            case 2048 -> tileColor = new Color(0xedc22e);
            default -> throw new IllegalArgumentException("This number is not allowed");
        }
        return tileColor;
    }

    @Override
    public Color getFrameColor() {
        return new Color(0, 0, 0, 0);
    }

    @Override
    public Color getBackgroundColor() {
        return Color.PINK;
    }

    @Override
    public Color getNonActiveGameColor() {
        return new Color(0, 0, 0, 128);
    }

    @Override
    public Color getTextColor() {
        return Color.WHITE;
    }
}
