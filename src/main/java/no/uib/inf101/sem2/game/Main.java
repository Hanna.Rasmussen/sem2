package no.uib.inf101.sem2.game;

import javax.swing.JFrame;

import no.uib.inf101.sem2.game.controller.GameController;
import no.uib.inf101.sem2.game.view.GameView;
import no.uib.inf101.sem2.model.GameBoard;
import no.uib.inf101.sem2.model.GameModel;

public class Main {
  public static void main(String[] args) {

    GameBoard board = new GameBoard(4, 4);
    GameModel model = new GameModel(board);
    GameView view = new GameView(model);
    @SuppressWarnings("unused")
    GameController controller = new GameController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101 2048");
    frame.setContentPane(view);
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }
}
