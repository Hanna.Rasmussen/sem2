package no.uib.inf101.sem2.game.view;

import java.awt.Color;

public interface ColorTheme {

    /**
     * @param value the value of the tile
     * 
     * @return the color of the tile
     */
    Color getTileColor(int value);

    /**
     * @return the color of the frame
     */
    Color getFrameColor();

    /**
     * @return the backgroundcolor
     */
    Color getBackgroundColor();

    /**
     * @return the color of the game when the game is not active
     */
    Color getNonActiveGameColor();

    /**
     * @return the color of the text on the non Active Game screen
     */
    Color getTextColor();

}
