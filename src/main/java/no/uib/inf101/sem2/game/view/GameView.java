package no.uib.inf101.sem2.game.view;

import javax.swing.JPanel;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.Tile;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class GameView extends JPanel {

    private final ViewableGameModel model;
    private final ColorTheme colors;
    private static final double margin = 7;
    private static final int width = 400;
    private static final int height = 400;

    public GameView(ViewableGameModel model) {

        colors = new DefaultColorTheme();
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(width, height));
        this.setBackground(colors.getBackgroundColor());
        this.model = model;

    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGame(g2);
    }

    private void drawGame(Graphics2D g2) {

        double width = this.getWidth() - 2 * margin;
        double height = this.getHeight() - 2 * margin;
        Rectangle2D r2 = new Rectangle2D.Double(margin, margin, width, height);
        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(r2, model.getGridDimension(), 2);

        drawTiles(g2, model.getTilesOnBoard(), converter, colors);
        if (model.getGameState() == GameState.START_GAME) {
            drawGrayScreenWithText(g2, converter, colors, "Press any key to start game");
        } else if (model.getGameState() == GameState.GAME_OVER) {
            drawGrayScreenWithText(g2, converter, colors, "Game Over");
        }
    }

    static void drawTiles(Graphics2D g2, Iterable<GridCell<Tile>> grid, CellPositionToPixelConverter converter,
            ColorTheme color) {

        for (GridCell<Tile> cell : grid) {
            Rectangle2D bounds = converter.getBoundsForCell(cell.pos());
            if (cell.value() == null) {
                g2.setColor(Color.BLACK);
            } else {
                g2.setColor(cell.value().getColor());
            }
            g2.fill(bounds);
            if (cell.value() != null) {
                g2.setColor(Color.BLACK);
                Inf101Graphics.drawCenteredString(g2, Integer.toString(cell.value().getValue()), bounds);
            }
        }
    }

    private void drawGrayScreenWithText(Graphics2D g2, CellPositionToPixelConverter converter, ColorTheme color,
            String text) {
        g2.setColor(color.getNonActiveGameColor());
        g2.fill(new Rectangle2D.Double(0, 0, getWidth(), getHeight()));
        g2.setColor(color.getTextColor());
        g2.setFont(new Font("Arial", Font.BOLD, 20));
        Inf101Graphics.drawCenteredString(g2, text, 0, 0, getWidth(), getHeight());

    }
}
