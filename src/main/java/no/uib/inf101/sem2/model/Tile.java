package no.uib.inf101.sem2.model;

import java.awt.Color;
import no.uib.inf101.sem2.game.view.ColorTheme;
import no.uib.inf101.sem2.game.view.DefaultColorTheme;

public class Tile {

    private int value;
    private Color tileColor;
    private ColorTheme colorTheme = new DefaultColorTheme();

    public Tile() {
        value = 0;
    }

    public Tile(int number) {
        value = number;
        setColor();

    }

    /**
     * @return the value of the tile
     */
    public int getValue() {
        return value;
    }

    /**
     * @param value sets the value of the tile
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * @param value sets the color of the tile
     */
    public void setColor() {
        tileColor = colorTheme.getTileColor(value);
    }

    /**
     * @return the color of the tile
     */
    public Color getColor() {
        return tileColor;
    }

    /**
     * @return the string representation of the tile
     */
    public String tileAString() {
        System.out.println("" + value);
        return "" + value;

    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Tile)) {
            return false;
        }
        Tile t = (Tile) o;
        return t.getValue() == value;
    }
}
