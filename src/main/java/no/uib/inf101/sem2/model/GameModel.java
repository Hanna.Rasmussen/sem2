package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.game.controller.ControllableGame;
import no.uib.inf101.sem2.game.view.ViewableGameModel;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;

public class GameModel implements ViewableGameModel, ControllableGame {

    private GameState gameState;
    private GameBoard board;

    public GameModel(GameBoard board) {
        this.board = board;
        this.gameState = GameState.START_GAME;
        placeRandomTile();

    }

    @Override
    public GridDimension getGridDimension() {
        return board;
    }

    @Override
    public Iterable<GridCell<Tile>> getTilesOnBoard() {
        return board;
    }

    @Override
    public GameState getGameState() {
        return this.gameState;
    }

    private Tile newTile() {
        if (Math.random() < 0.9) {
            return new Tile(2);
        } else {
            return new Tile(4);
        }
    }

    @Override
    public void placeRandomTile() {
        int x = (int) (Math.random() * board.rows());
        int y = (int) (Math.random() * board.cols());
        if (!boardIsFull()) {
            if (board.get(new CellPosition(x, y)) == null) {
                board.set(new CellPosition(x, y), newTile());
            } else {
                placeRandomTile();
            }
        }
    }

    private boolean isLegalMove(CellPosition pos) {
        return board.positionIsOnGrid(pos) && board.get(pos) == null;
    }

    private void combine(CellPosition pos1, CellPosition pos2) {
        board.set(pos1, null);
        board.set(pos2, new Tile(board.get(pos2).getValue() * 2));
    }

    private boolean legalCombinations() {
        for (int row = 0; row < board.rows(); row++) {
            for (int col = 0; col < board.cols(); col++) {
                if (isLegalMove(new CellPosition(row, col))) {
                    continue;
                }
                if (row > 0 && board.get(new CellPosition(row, col)).equals(board
                        .get(new CellPosition(row - 1, col)))) {
                    return true;
                }
                if (row < board.rows() - 1 && board.get(new CellPosition(row, col)).equals(board
                        .get(new CellPosition(row + 1, col)))) {
                    return true;
                }
                if (col > 0 && board.get(new CellPosition(row, col)).equals(board
                        .get(new CellPosition(row, col - 1)))) {
                    return true;
                }
                if (col < board.cols() - 1 && board.get(new CellPosition(row, col)).equals(board
                        .get(new CellPosition(row, col + 1)))) {
                    return true;
                }
            }
        }
        return false;
    }

    private void combineIfPossible(String direction) {
        for (int row = 0; row < board.rows(); row++) {
            for (int col = 0; col < board.cols(); col++) {
                if (isLegalMove(new CellPosition(row, col))) {
                    continue;
                }
                switch (direction) {
                    case "up":
                        if (row > 0 && board.get(new CellPosition(row, col)).equals(board
                                .get(new CellPosition(row - 1, col)))) {
                            combine(new CellPosition(row, col), new CellPosition(row - 1, col));
                        }
                        break;
                    case "down":
                        if (row < board.rows() - 1 && board.get(new CellPosition(row, col)).equals(board
                                .get(new CellPosition(row + 1, col)))) {
                            combine(new CellPosition(row, col), new CellPosition(row + 1, col));
                        }
                        break;
                    case "left":
                        if (col > 0 && board.get(new CellPosition(row, col)).equals(board
                                .get(new CellPosition(row, col - 1)))) {
                            combine(new CellPosition(row, col), new CellPosition(row, col - 1));
                        }
                        break;
                    case "right":
                        if (col < board.cols() - 1 && board.get(new CellPosition(row, col)).equals(board
                                .get(new CellPosition(row, col + 1)))) {
                            combine(new CellPosition(row, col), new CellPosition(row, col + 1));
                        }
                        break;
                    default:
                        throw new IllegalArgumentException("Invalid direction: " + direction);
                }

            }

        }

        if (isGameOver()) {
            this.gameState = GameState.GAME_OVER;
        }
    }

    @Override
    public void startGame() {
        gameState = GameState.ACTIVE_GAME;
    }

    @Override
    public void moveUp() {
        for (int row = 0; row < board.rows(); row++) {
            for (int col = 0; col < board.cols(); col++) {
                if (board.get(new CellPosition(row, col)) == null) {
                    continue;
                }
                int emptyTiles = 1;
                while (isLegalMove(new CellPosition(row - emptyTiles, col))) {
                    board.set(new CellPosition(row - emptyTiles, col),
                            board.get(new CellPosition(row - emptyTiles + 1, col)));
                    board.set(new CellPosition(row - emptyTiles + 1, col), null);
                    ++emptyTiles;
                }
                combineIfPossible("up");
            }

        }

    }

    @Override
    public void moveDown() {
        for (int row = board.rows() - 1; row >= 0; row--) {
            for (int col = 0; col < board.cols(); col++) {
                if (board.get(new CellPosition(row, col)) == null) {
                    continue;
                }
                int emptyTiles = 1;
                while (isLegalMove(new CellPosition(row + emptyTiles, col))) {
                    board.set(new CellPosition(row + emptyTiles, col),
                            board.get(new CellPosition(row + emptyTiles - 1, col)));
                    board.set(new CellPosition(row + emptyTiles - 1, col), null);
                    ++emptyTiles;
                }
                combineIfPossible("down");
            }

        }

    }

    @Override
    public void moveLeft() {
        for (int i = 0; i < board.rows(); i++) {
            for (int j = 0; j < board.cols(); j++) {
                if (board.get(new CellPosition(i, j)) == null) {
                    continue;
                }
                int counter = 1;
                while (isLegalMove(new CellPosition(i, j - counter))) {
                    board.set(new CellPosition(i, j - counter), board.get(new CellPosition(i, j - counter + 1)));
                    board.set(new CellPosition(i, j - counter + 1), null);
                    ++counter;
                }
                combineIfPossible("left");
            }

        }

    }

    @Override
    public void moveRight() {
        for (int i = 0; i < board.rows(); i++) {
            for (int j = board.cols() - 1; j >= 0; j--) {
                if (board.get(new CellPosition(i, j)) == null) {
                    continue;
                }
                int counter = 1;
                while (isLegalMove(new CellPosition(i, j + counter))) {
                    board.set(new CellPosition(i, j + counter), board.get(new CellPosition(i, j + counter - 1)));
                    board.set(new CellPosition(i, j + counter - 1), null);
                    ++counter;
                }
                combineIfPossible("right");
            }

        }

    }

    private boolean boardIsFull() {
        for (int row = 0; row < board.rows(); row++) {
            for (int col = 0; col < board.cols(); col++) {
                if (board.get(new CellPosition(row, col)) == null) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean legalMovesLeft() {
        if (boardIsFull() && !legalCombinations()) {
            return false;
        }
        return true;
    }

    private boolean isGameOver() {
        return !legalMovesLeft();
    }
}
