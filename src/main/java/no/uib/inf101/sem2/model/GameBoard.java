package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;

public class GameBoard extends Grid<Tile> {

    public GameBoard(int rows, int cols) {
        super(rows, cols, null);
    }

    /**
     * turns the board to a string with each row on a separate line, and each cell
     * with its value
     * 
     * @return the board as a string,
     */
    public String prettyString() {

        String result = "";
        for (int row = 0; row < rows(); row++) {
            for (int col = 0; col < cols(); col++) {
                Tile tile = get(new CellPosition(row, col));
                if (tile == null) {
                    result += "0";
                } else {
                    result += tile.getValue();
                }

            }
            result += "\n";
        }
        return result.trim();

    }
}
