package no.uib.inf101.sem2.model;

public enum GameState {
    START_GAME, ACTIVE_GAME, GAME_OVER,
}
