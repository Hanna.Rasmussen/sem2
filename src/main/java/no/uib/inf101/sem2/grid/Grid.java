package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;

import no.uib.inf101.sem2.model.Tile;

public class Grid<E> implements IGrid<Tile> {

    private int rows = 4;
    private int cols = 4;
    private ArrayList<ArrayList<Tile>> grid;

    public Grid(int rows, int cols) {
        this(rows, cols, null);
    }

    public Grid(int rows, int cols, Tile defaultValue) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<ArrayList<Tile>>(rows);

        for (int i = 0; i < rows; i++) {
            ArrayList<Tile> list = new ArrayList<Tile>(cols);
            for (int j = 0; j < cols; j++) {
                list.add(defaultValue);
            }
            grid.add(list);
        }

    }

    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int cols() {
        return cols;
    }

    @Override
    public Iterator<GridCell<Tile>> iterator() {
        ArrayList<GridCell<Tile>> list = new ArrayList<GridCell<Tile>>();

        for (int i = 0; i < rows(); i++) {
            for (int j = 0; j < cols(); j++) {
                list.add(new GridCell<Tile>(new CellPosition(i, j), get(new CellPosition(i, j))));
            }
        }
        return list.iterator();
    }

    @Override
    public void set(CellPosition pos, Tile value) {
        this.grid.get(pos.row()).set(pos.col(), value);
    }

    @Override
    public Tile get(CellPosition pos) {
        int x = pos.row();
        int y = pos.col();
        return this.grid.get(x).get(y);
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        return (pos.row() >= 0) && (pos.row() < this.rows()) && (pos.col() >= 0) && (pos.col() < this.cols());
    }

}
