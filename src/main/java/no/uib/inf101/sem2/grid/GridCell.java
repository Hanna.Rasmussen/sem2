package no.uib.inf101.sem2.grid;

public record GridCell<Tile>(CellPosition pos, Tile value) {

}
