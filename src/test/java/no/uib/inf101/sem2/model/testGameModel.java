package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;

public class testGameModel {

    @Test
    public void testMoveUp() {

        GameBoard board = new GameBoard(4, 4);
        GameModel model = new GameModel(board);

        board.set(new CellPosition(0, 0), new Tile(2));
        board.set(new CellPosition(1, 0), new Tile(2));
        board.set(new CellPosition(2, 0), new Tile(4));
        board.set(new CellPosition(3, 0), null);
        board.set(new CellPosition(0, 1), null);
        board.set(new CellPosition(1, 1), new Tile(16));
        board.set(new CellPosition(2, 1), null);
        board.set(new CellPosition(3, 1), null);

        model.moveUp();

        // Can not check if the tiles are null because of the random placement of the
        // new tiles
        assertEquals(new Tile(8), board.get(new CellPosition(0, 0)));
        assertEquals(new Tile(16), board.get(new CellPosition(0, 1)));
    }

    @Test
    public void testMoveDown() {

        GameBoard board = new GameBoard(4, 4);
        GameModel model = new GameModel(board);

        board.set(new CellPosition(0, 0), new Tile(2));
        board.set(new CellPosition(1, 0), new Tile(2));
        board.set(new CellPosition(2, 0), null);
        board.set(new CellPosition(3, 0), null);
        board.set(new CellPosition(0, 1), new Tile(4));
        board.set(new CellPosition(1, 1), null);
        board.set(new CellPosition(2, 1), null);
        board.set(new CellPosition(3, 1), null);

        model.moveDown();

        assertEquals(new Tile(4), board.get(new CellPosition(3, 0)));
        assertEquals(new Tile(4), board.get(new CellPosition(3, 1)));
    }

    @Test
    public void testMoveLeft() {

        GameBoard board = new GameBoard(4, 4);
        GameModel model = new GameModel(board);

        board.set(new CellPosition(0, 0), new Tile(2));
        board.set(new CellPosition(0, 1), new Tile(2));
        board.set(new CellPosition(0, 2), null);
        board.set(new CellPosition(0, 3), null);
        board.set(new CellPosition(1, 3), new Tile(4));
        board.set(new CellPosition(1, 2), null);
        board.set(new CellPosition(1, 1), null);
        board.set(new CellPosition(1, 0), null);

        model.moveLeft();

        assertEquals(new Tile(4), board.get(new CellPosition(0, 0)));
        assertEquals(new Tile(4), board.get(new CellPosition(1, 0)));
    }

    @Test
    public void testMoveRight() {

        GameBoard board = new GameBoard(4, 4);
        GameModel model = new GameModel(board);

        board.set(new CellPosition(0, 0), new Tile(2));
        board.set(new CellPosition(0, 1), new Tile(2));
        board.set(new CellPosition(0, 2), null);
        board.set(new CellPosition(0, 3), null);
        board.set(new CellPosition(1, 2), new Tile(4));
        board.set(new CellPosition(1, 3), null);
        board.set(new CellPosition(1, 1), null);
        board.set(new CellPosition(1, 0), null);

        model.moveRight();

        assertEquals(new Tile(4), board.get(new CellPosition(0, 3)));
        assertEquals(new Tile(4), board.get(new CellPosition(1, 3)));
    }
}
