package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;

public class testGameBoard {

    @Test
    public void testPrettyString() {
        GameBoard board = new GameBoard(3, 4);
        board.set(new CellPosition(0, 0), new Tile(2));
        board.set(new CellPosition(0, 3), new Tile(16));
        board.set(new CellPosition(2, 0), new Tile(2048));
        board.set(new CellPosition(2, 3), new Tile(4));
        String expected = String.join("\n", new String[] { "20016", "0000", "2048004" });
        assertEquals(expected, board.prettyString());
    }

}