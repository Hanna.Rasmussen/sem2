# 2048 

The program I have made is a version of 2048. This is a game where the point is to combine tiles with numbers until you get the number 2048. The tiles value start at 2. You combine tiles by moving all the tiles on the board in the same direction and if the tiles next to eachother are equal they will combine and their value will double.

You can move the board up, down, left and right, and all the tiles will move as far as they can in that direction. Every time you move the board a new tile will be randomly placed in an empty space on the board. There is a 90% chance you will get the tile 2 and 10% chance you will get the tile 4. 

Here is a video of me showing and explaining the game to my friend:
https://youtu.be/CGmAZLZNYKA


